<?php

namespace Drupal\d01_drupal_social\Element;

use Drupal\Core\Render\Element\RenderElement;
use Drupal\Core\Url;

/**
 * D01 social share elements.
 *
 * @RenderElement("d01_drupal_social_share_buttons")
 */
class ElementSocialShareButtons extends RenderElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);

    return [
      '#theme' => 'd01_drupal_social_share_buttons',
      '#attributes' => [
        'class' => ['js-share--social'],
      ],
      '#pre_render' => [
        [$class, 'preRenderElement'],
      ],
      '#cache' => [
        'max-age' => 0,
      ],
    ];
  }

  /**
   * Prepare the render array for the template.
   */
  public static function preRenderElement($element) {
    // Attach JS library.
    $element['#attached']['library'][] = 'd01_drupal_social/social_share';

    return $element;
  }

}
