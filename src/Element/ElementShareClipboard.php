<?php

namespace Drupal\d01_drupal_social\Element;

use Drupal\Core\Render\Element\RenderElement;
use Drupal\Core\Url;

/**
 * D01 share on clipboard element.
 *
 * @RenderElement("d01_drupal_social_clipboard")
 */
class ElementShareClipboard extends RenderElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);
    return [
      '#theme' => 'd01_drupal_social_clipboard',
      '#attributes' => [
        'class' => 'js-clipboard',
      ],
      '#pre_render' => [
        [$class, 'preRenderElement'],
      ],
      '#cache' => [
        'max-age' => 0,
      ],
    ];
  }

  /**
   * Prepare the render array for the template.
   */
  public static function preRenderElement($element) {
    return $element;
  }

}
