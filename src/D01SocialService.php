<?php

namespace Drupal\d01_drupal_social;

use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Routing\CurrentRouteMatch;

/**
 * Class D01SocialService.
 *
 * @package Drupal\d01_drupal_social
 */
class D01SocialService implements D01SocialServiceInterface {

  /**
   * Facebook base share URL.
   */
  const FACEBOOK_URL = 'https://www.facebook.com/sharer/sharer.php';

  /**
   * Twitter base share URL.
   */
  const TWITTER_URL = 'https://twitter.com/intent/tweet/';

  /**
   * LinkedIn base share URL.
   */
  const LINKEDIN_URL = 'https://www.linkedin.com/shareArticle';

  /**
   * Drupal\Core\Routing\CurrentRouteMatch definition.
   *
   * @var \Drupal\Core\Routing\CurrentRouteMatch
   */
  protected $currentRouteMatch;

  /**
   * Constructs the Social Service.
   *
   * @param \Drupal\Core\Routing\CurrentRouteMatch $current_route_match
   *   The current route match.
   */
  public function __construct(CurrentRouteMatch $current_route_match) {
    $this->currentRouteMatch = $current_route_match;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_route_match')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getEntity() {
    $entity = NULL;

    // Retrieve current entity.
    foreach ($this->currentRouteMatch->getParameters() as $parameter) {
      if ($parameter instanceof EntityInterface) {
        $entity = $this->currentRouteMatch->getParameter($parameter->getEntityTypeId());
      }
    }

    return $entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityUrl() {
    // Retrieve current entity.
    $entity = $this->getEntity();

    // Retrieve shareable URL based on entity or current route.
    if ($entity) {
      $share_url = $entity->toUrl('canonical', ['absolute' => TRUE])->toString();
    }
    else {
      $share_url = Url::fromRoute('<current>', [], ['absolute' => 'true'])->toString();
    }

    return $share_url;
  }

  /**
   * {@inheritdoc}
   */
  public function getShareableUrl($type, $share_url = NULL, $prefix = NULL) {
    // URL to share.
    $share_url = $share_url ? $share_url : $this->getEntityUrl();

    // Set default share options.
    $options = [
      'absolute' => TRUE,
      'external' => TRUE,
      'query' => [],
    ];

    // Get social media base share URL + set options.
    switch ($type) {
      case 'facebook':
        $sharer = self::FACEBOOK_URL;

        $options['query'] = [
          'u' => $share_url,
        ];
        break;

      case 'twitter':
        $sharer = self::TWITTER_URL;

        $options['query'] = [
          'url' => $share_url,
          'text' => $prefix ? $prefix . ' ' . $this->getTitle() : $this->getTitle(),
        ];
        break;

      case 'linkedin':
        $sharer = self::LINKEDIN_URL;

        $options['query'] = [
          'mini' => 'true',
          'url' => $share_url,
          'title' => $prefix ? $prefix . ' ' . $this->getTitle() : $this->getTitle(),
        ];
        break;

      case 'mail':
        $sharer = NULL;
        $share_url = 'mailto:?subject=' . $this->getTitle() . '&body=' . $this->getEntityUrl();
        break;

      default:
        $sharer = NULL;
        break;
    }

    return $sharer ? Url::fromUri($sharer, $options) : $share_url;
  }

  /**
   * {@inheritdoc}
   */
  public function getTitle() {
    $entity = $this->getEntity();

    if ($entity) {
      return $entity->__isset('title') ? $entity->getTitle() : $entity->getName();
    }

    return $entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getLinkAttributes($title) {
    $attributes = [
      'data-popup-width' => 600,
      'data-popup-height' => 300,
      'title' => $title,
    ];

    return $attributes;
  }

  /**
   * {@inheritdoc}
   */
  public function getIcon($social_key) {
    // Social media key.
    $key = 'd01_drupal_social_' . $social_key . '_attributes';

    // Get config.
    $d01_drupal_social_settings = \Drupal::config('d01_drupal_social_attributes.settings');
    $icon_class = $d01_drupal_social_settings->get($key);

    return $icon_class;
  }

}
