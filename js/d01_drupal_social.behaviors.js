(function ($, Drupal) {

  'use strict';

  /**
   * Behavior to generate social share pop-up.
   */
  Drupal.behaviors.social_share = {
    attach: function(context, settings) {
      $(".js-share--social a").not('.js-clipboard').on('click', function (e) {
        e.preventDefault();

        // Get pop-up dimensions.
        var h = $(this).data("popup-height"),
            w = $(this).data("popup-width");

        // Get window sizes.
        var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
        var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

        // Calculate screen center.
        var left = ((width / 2) - (w / 2));
        var top = ((height / 2) - (h / 2));

        // Pop up.
        window.open(
          $(this).attr("href"),
          "share",
          "top=" + top + ",left=" + left + ",width=" + w + ",height=" + h
        );
      });
    }
  };

  /**
   * Behavior to copy URL to clipboard.
   */
  Drupal.behaviors.social_share_clipboard = {
    attach: function(context, settings) {
      $(".js-share--social .js-clipboard").on('click', function (e) {
        e.preventDefault();

        CopyToClipboard('clipboard-copy');

      });
    }
  };

  /**
   * Copy URL to clipboard function.
   *
   * @param wrapper_id
   */
  function CopyToClipboard(wrapper_id) {
    // @TODO:: check cross browser compatibility.
    var range = document.createRange();
    range.selectNode(document.getElementById(wrapper_id));
    window.getSelection().addRange(range);
    document.execCommand("copy");
  }

})(jQuery, Drupal);
