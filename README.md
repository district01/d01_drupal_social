# D01 Drupal social share module

## Technical details
Social share elements usage:
```
$socialService = \Drupal::service('d01_drupal_social.generator');

$facebook = [
  '#type' => 'd01_drupal_social_facebook',
  '#share_url' => $socialService->getShareableUrl('facebook'),
  '#attributes' => $socialService->getLinkAttributes('facebook'),
];

$clipboard = [
  '#type' => 'd01_drupal_social_clipboard',
  '#share_url' => $socialService->getShareableUrl('cb'),
];

$variables['social_media_elements'] = [
  '#type' => 'd01_drupal_social_share_buttons',
  '#social_share' => [$facebook, $clipboard],
];
```

Element attributes can easily be overwritten. By default: #share_url will share the current Entity URL, #attributes will contain pop-up dimensions (600x300) and the current Entity title.