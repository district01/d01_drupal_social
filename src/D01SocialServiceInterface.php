<?php

namespace Drupal\d01_drupal_social;

/**
 * Interface D01SocialServiceInterface.
 *
 * @package Drupal\d01_drupal_social
 */
interface D01SocialServiceInterface {

  /**
   * Retrieve current entity using CurrentRouteMatch.
   *
   * @return \Drupal\Core\Entity\EntityBase
   *   Returns the current entity.
   */
  public function getEntity();

  /**
   * Retrieve the current Entity its URL.
   *
   * @return string
   *   Returns a shareable URL.
   */
  public function getEntityUrl();

  /**
   * Build shareable URL for given social media type.
   *
   * @param string $type
   *   Social media type to build shareable url for.
   * @param bool $share_url
   *   Default url to be shared, if not given we use url for current entity.
   * @param mixed $prefix
   *   Text prefix.
   *
   * @return mixed
   *   Returns URL object.
   */
  public function getShareableUrl($type, $share_url = NULL, $prefix = NULL);

  /**
   * Retrieve the current entity title.
   *
   * @return string
   *   Returns the title for current entity.
   */
  public function getTitle();

  /**
   * Retrieve the shareable link attributes.
   *
   * @return array
   *   Returns an array containing shareable link attributes.
   */
  public function getLinkAttributes($network_name);

  /**
   * Retrieve a social service icon.
   *
   * @return string
   *   Returns class of social service icon.
   */
  public function getIcon($social_key);

}
