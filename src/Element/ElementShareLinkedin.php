<?php

namespace Drupal\d01_drupal_social\Element;

use Drupal\Core\Render\Element\RenderElement;

/**
 * D01 share on linked in element.
 *
 * @RenderElement("d01_drupal_social_linkedin")
 */
class ElementShareLinkedin extends RenderElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);

    return [
      '#theme' => 'd01_drupal_social_linkedin',
      '#pre_render' => [
        [$class, 'preRenderElement'],
      ],
      '#cache' => [
        'max-age' => 0,
      ],
    ];
  }

  /**
   * Prepare the render array for the template.
   */
  public static function preRenderElement($element) {
    return $element;
  }

}
